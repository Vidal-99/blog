'use client'

import { headerNavLinks, headerNavLinksAuth } from '@/src/lib/navLinks'
import Link from 'next/link'
import { signOut, useSession } from "next-auth/react";
import { useEffect } from "react";
import { checkSessionStatus } from "@/src/lib/data";

const Header = () => {

    const { data: session, status } = useSession();
    const checkSessionIntervalMS = 300000;    

    useEffect(() => {
        if (session?.user) {
            const interval = setInterval(() => {
                checkSessionStatus(session?.user?.token).catch(() => {
                    signOut(); 
                });
            }, checkSessionIntervalMS);
            return () => clearInterval(interval);
        }
    }, [session?.user]);

    const handleSignOut = async () => {
        await signOut({ redirect: false });
    };

    useEffect(() => {
        if (session?.user) {
            const interval = setInterval(() => {
                checkSessionStatus(session?.user?.token).catch(() => {
                    signOut();
                });
            }, checkSessionIntervalMS);
            return () => clearInterval(interval);
        }
    }, [session?.user]);

    return (
        <header className="flex items-center justify-between py-10">
            <div className='flex items-center space-x-4 leading-5 sm:space-x-6'>
                <Link href="/">
                    <div className="flex items-center justify-between">
                        <div className="mr-3">
                            Logo
                        </div>
                    </div>
                </Link>
                {headerNavLinks
                    .filter((link) => link.href !== '/')
                    .map((link) => (
                        <Link
                            key={link.title}
                            href={link.href}
                            className="hidden font-medium text-gray-900 hover:text-primary-500 dark:text-gray-100 dark:hover:text-primary-400
              sm:block"
                        >
                            {link.title}
                        </Link>
                    ))}
            </div>
            <div className="flex items-center space-x-4 leading-5 sm:space-x-6">
                {session?.user ? (
                    <>
                        <p className="hidden font-medium text-gray-900 hover:text-primary-500 dark:text-gray-100 dark:hover:text-primary-400 sm:block">
                            Bienvenue {session.user.name}
                        </p>
                        <button
                            onClick={handleSignOut}
                            className="hidden font-medium text-gray-900 hover:text-primary-500 dark:text-gray-100 dark:hover:text-primary-400 sm:block"
                        >
                            Se déconnecter
                        </button>
                    </>
                ) : (
                    headerNavLinksAuth.map((link) => (
                        <Link
                            key={link.title}
                            href={link.href}
                            className={link.title === "Signin" ? "hidden font-medium text-gray-900 hover:text-primary-500 dark:text-gray-100 dark:hover:text-primary-400 sm:block" : 'flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600'}
                        >
                            {link.title}
                        </Link>
                    ))
                )}
            </div>
        </header>
    )
}

export default Header
