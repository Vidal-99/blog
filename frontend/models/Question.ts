import { z } from "zod";
import { UserSchema } from "./User";

export const QuestionSchema = z.object({
  title: z.string().min(1),
  content: z.string().min(1),
  createdAt: z.string(),
  imgSrc: z.string(),
  modifiedAt: z.string(),
  author: UserSchema,
});

export type Question = z.infer<typeof QuestionSchema>;
