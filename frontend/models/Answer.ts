import { z } from "zod";
import { UserSchema } from "./User";

export const AnswerSchema = z.object({
  content: z.string().min(1),
  createdAt: z.string(),
  modifiedAt: z.string(),
  author: UserSchema,
  questionId: z.number(),
});

export type Answer = z.infer<typeof AnswerSchema>;
