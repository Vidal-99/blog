import { unstable_noStore } from "next/cache";

// USERS ENDPOINTS
export async function registerUser(
  name: string,
  email: string,
  password: string,
) {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}auth/signup`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        email,
        password,
      }),
    });
    const data = await res.json();
    return data;
  } catch (err) {
      console.log(err);
      
    throw new Error("Failed to register the user");
  }
}


export async function getAllQuestions() {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}questions`, {
      method: "GET",
    });

    if (!res.ok) {
      throw new Error(
        `Failed to fetch questions: ${res.status} ${res.statusText}`
      );
    }

    const data = await res.json();
    return data;
  } catch (error) {
    console.error("Failed to fetch questions:", error);
    throw new Error("Unexpected error");
  }
}


export async function checkSessionStatus(token: string) {
  try {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}auth/check`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await res.json();
    if (data.code === 401) throw new Error("Expired JWT token");
  } catch (err) {
    throw new Error("Unexpected error");
  }
}