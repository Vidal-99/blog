export const headerNavLinks = [
  { href: "/", title: "Home" },
  { href: "/blog", title: "Blog" },
  { href: "/portfolio", title: "Portfolio" },
  { href: "/contact", title: "Contact" },
];

export const headerNavLinksAuth = [
  { href: "/auth/signin", title: "Signin" },
  { href: "/auth/signup", title: "Signup" },
];
