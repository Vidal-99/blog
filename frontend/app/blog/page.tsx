'use client'

import React, { useEffect, useState } from 'react';
import Card from '@/components/Card';
import { getAllQuestions } from '@/src/lib/data';
import { Question, QuestionSchema } from '@/models/Question'; 

export default function Page() {
    const [questions, setQuestions] = useState<Question[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);

    useEffect(() => {
        const fetchQuestions = async () => {
            try {
                const questionsData = await getAllQuestions();
                console.log(questionsData);
                
                const validatedQuestions = questionsData.map((question: any) => {
                    return QuestionSchema.parse(question);
                });
                setQuestions(validatedQuestions);
            } catch (error) {
                console.error("Failed to fetch questions:", error);
                setError("Failed to fetch questions.");
            } finally {
                setIsLoading(false);
            }
        };

        fetchQuestions();
    }, []);

    if (isLoading) {
        return <p>Loading...</p>;
    }

    if (error) {
        return <p>{error}</p>;
    }

    return (
        <>
            <div className="divide-y divide-gray-200 dark:divide-gray-700">
                <div className="space-y-2 pb-8 pt-6 md:space-y-5">
                    <h1 className="text-3xl font-extrabold leading-9 tracking-tight text-gray-900 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-6xl md:leading-14">
                        Projects
                    </h1>
                    <p className="text-lg leading-7 text-gray-500 dark:text-gray-400">
                        Showcase your projects with a hero image (16 x 9)
                    </p>
                </div>
                <div className="container py-12">
                    <div className="-m-4 flex flex-wrap">
                        {questions.map((question) => (
                            <Card
                                key={question.title}
                                question={question}
                            />
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
}
