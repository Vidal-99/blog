"use client";

import { SessionProvider, SessionProviderProps } from "next-auth/react";
import { ReactNode } from "react";

interface Props {
    children: ReactNode
}

export function SessionAuthProvider({children}: Props) {    
    return <SessionProvider>{children}</SessionProvider>;
}
