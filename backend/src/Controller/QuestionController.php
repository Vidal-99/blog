<?php

namespace App\Controller;

use App\Entity\Question;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/api/questions')]
class QuestionController extends AbstractController 
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SerializerInterface $serializer,
        private readonly ValidatorInterface $validator,
        private readonly QuestionRepository $questionRepository
    ){
    }

    #[Route('new', name: 'question_create', methods: ['POST'])]
    public function create(
        Request $request,
        
        EntityManagerInterface $em
    ): JsonResponse {
        $user = $this->getUser();

        if (!$user) {
            return new JsonResponse(['message' => 'Unauthorized'], Response::HTTP_UNAUTHORIZED);
        }

        $question = $this->serializer->deserialize($request->getContent(), Question::class, 'json');
        $question->setAuthor($user);

        $errors = $this->validator->validate($question);
        if (count($errors) > 0) {
            $errorMessages = array_map(fn ($error) => [
                'field' => $error->getPropertyPath(),
                'message' => $error->getMessage()
            ], iterator_to_array($errors));

            return $this->json([
                'success' => false,
                'message' => 'Validation errors occurred.',
                'errors' => $errorMessages
            ], Response::HTTP_BAD_REQUEST);
        }

        $em->persist($question);
        $em->flush();

        return $this->json($question, Response::HTTP_CREATED, [], ['groups' => 'read']);
    }

}