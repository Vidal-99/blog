<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/api/auth', name: 'api_auth_')]
class AuthController extends AbstractController
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly ValidatorInterface $validator,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly EntityManagerInterface $em,
        private readonly NormalizerInterface $normalizer
    ) {
        
    }

    #[Route('/signup', name: 'signup', methods: ['POST'])]
    public function signup(Request $request): JsonResponse
    {
        $newUser = $this->serializer->deserialize($request->getContent(), User::class, 'json');
        $errors = $this->validator->validate($newUser);

        if (count($errors) > 0) {
            return $this->json([
                'success' => false,
                'message' => 'Une erreur s\'est produite lors de la création du compte.',
                'errors' => array_map(fn ($error) => [
                    'field' => $error->getPropertyPath(),
                    'message' => $error->getMessage()
                ], (array) $errors)
            ]);
        }

        $newUser->setPassword(
            $this->passwordHasher->hashPassword($newUser, $newUser->getPassword())
        );

        $this->em->persist($newUser);
        $this->em->flush();

        $normalizedUser = $this->normalizer->normalize($newUser, null, ['groups' => 'read']);

        return new JsonResponse(['user' => $normalizedUser], Response::HTTP_CREATED);
    }

    #[Route('/auth/check', name: 'auth_check', methods: ['GET'])]
    public function checkSessionStatus(): Response
    {
        return new Response(true, Response::HTTP_OK);
    }
}
