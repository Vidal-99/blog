<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\AnswerRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AnswerRepository::class)]
#[ApiResource(
    operations: [
        new Get(normalizationContext: ['groups' => ['answer:read']]),
        new GetCollection(normalizationContext: ['groups' => ['answer:list']]),
        new Patch(
            normalizationContext: ['groups' => ['answer:read']],
            denormalizationContext: ['groups' => ['answer:write']],
            security: 'object.getUser() == user'
        ),
        new Post(
            denormalizationContext: ['groups' => ['answer:write']],
            normalizationContext: ['groups' => ['answer:read']]
        )
    ]
)]
class Answer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['answer:read', 'answer:list', 'user:list', 'user:read'])]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['answer:read', 'answer:list', 'answer:write', 'user:read'])]
    private ?string $content = null;

    #[ORM\Column]
    #[Groups(['answer:read', 'answer:list', 'answer:write', 'user:read'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    #[Groups(['answer:read', 'answer:list', 'answer:write', 'user:read'])]
    private ?\DateTimeImmutable $modifiedAt = null;

    #[ORM\ManyToOne(inversedBy: 'answers')]
    #[Groups(['answer:read', 'answer:list', 'answer:write'])]
    private ?User $author = null;

    #[ORM\ManyToOne(inversedBy: 'answers')]
    #[Groups(['answer:read', 'answer:list'])]
    private ?Question $Question = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->modifiedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(\DateTimeImmutable $modifiedAt): static
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->Question;
    }

    public function setQuestion(?Question $Question): static
    {
        $this->Question = $Question;

        return $this;
    }
}
