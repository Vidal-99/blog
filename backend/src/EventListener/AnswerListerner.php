<?php

namespace App\EventListener;

use App\Entity\Answer;
use Doctrine\ORM\Events;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;

#[AsEntityListener(event: Events::prePersist, entity: Answer::class)]
#[AsEntityListener(event: Events::preUpdate, entity: Answer::class)]
class AnswerListener
{
    public function __construct(
        private readonly Security $security,
    ) {
    }

    public function prePersist(Answer $answer, LifecycleEventArgs $event): void
    {
        $answer->setAuthor($this->security->getUser());
    }

    public function preUpdate(Answer $answer, LifecycleEventArgs $event): void
    {
        $answer->setModifiedAt(new \DateTimeImmutable());
    }
}
