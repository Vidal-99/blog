<?php

namespace App\EventListener;

use App\Entity\Question;
use Doctrine\ORM\Events;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;

#[AsEntityListener(event: Events::prePersist, entity: Question::class)]
#[AsEntityListener(event: Events::preUpdate, entity: Question::class)]
class QuestionListener
{
    public function __construct(
        private readonly Security $security,
    ) {
    }

    public function prePersist(Question $question, LifecycleEventArgs $event): void
    {
        $question->setAuthor($this->security->getUser());
    }

    public function preUpdate(Question $question, LifecycleEventArgs $event): void
    {
        $question->setModifiedAt(new \DateTimeImmutable());
    }
}
