.PHONY: help
.DEFAULT_GOAL := help

help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@echo "  up          : Start the application"
	@echo "  down        : Stop the application"
	@echo "  build       : Build Docker images"
	@echo "  restart     : Restart the application"
	@echo "  logs        : View container logs"
	@echo "  clean       : Remove all containers and volumes"
	@echo "  ps          : List containers"
	@echo "  install     : Install Composer dependencies"
	@echo "  db-create   : Create the database"
	@echo "  db-drop     : Drop the database"
	@echo "  db-update   : Update the database schema"
	@echo "  db-migrate  : Execute database migrations"
	@echo "  bash        : Open a bash session in the php container"
	@echo "  stop        : Stop the containers"
	@echo ""

start:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

restart: down start

logs:
	docker-compose logs -f

clean:
	docker-compose down -v --remove-orphans

ps:
	docker-compose ps

install:
	docker-compose run --rm php composer install

bash:
	docker-compose exec php bash

stop:
	docker-compose stop

db-create:
	docker-compose run --rm php symfony console doctrine:database:create

db-drop:
	docker-compose run --rm php symfony console doctrine:database:drop --force

db-update:
	docker-compose run --rm php symfony console doctrine:schema:update --force

db-migration:
	docker-compose run --rm php symfony console make:migration

db-migrate:
	docker-compose run --rm php symfony console doctrine:migrations:migrate

load-fixture:
	docker-compose run --rm php symfony console doctrine:fixtures:load